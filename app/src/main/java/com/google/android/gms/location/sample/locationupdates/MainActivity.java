package com.google.android.gms.location.sample.locationupdates;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

/**
 * Created by Suuny on 11/4/2015.
 */
public class MainActivity extends Activity implements View.OnClickListener {

    private TextView mStartLatLonTextView;
  private TextView mLastLatLonTextView;

    // Labels.
    private String mLatitudeLongitude=null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        // Locate the UI widgets.

        mStartLatLonTextView = (TextView) findViewById(R.id.start_lat_lon_text);
        mLastLatLonTextView = (TextView) findViewById(R.id.last_lat_lon_text);

    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.start_lat_button)
        {
           mLatitudeLongitude=SecondActivity.getLatLon() ;
          mStartLatLonTextView.setText(mLatitudeLongitude);
        }
        else if (v.getId()==R.id.last_lat_button)
        {
            mLatitudeLongitude=SecondActivity.getLatLon() ;
            mLastLatLonTextView.setText(mLatitudeLongitude);

        }
    }
}
